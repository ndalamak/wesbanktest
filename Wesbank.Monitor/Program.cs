﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using Wesbank.Shared.Models;
using Wesbank.Shared.Repository;
using System.ServiceProcess;

namespace Wesbank.Monitor
{
    public class XmlWatcher
    {
        private static string InComingXmlFolder;
        private static string LogFile;
        private static IMarketer _marketersRepository;
       
        static void Main(string[] args)
        {
            var container = new UnityContainer();
            container.RegisterType<IMarketer, Repository.Marketers.Repository>();
            _marketersRepository = container.Resolve<IMarketer>();

            var inComingXmlFolder = ConfigurationManager.AppSettings["Incoming_XML_Folder"];
            var logFile = ConfigurationManager.AppSettings["LogSavePath"];

            InComingXmlFolder = inComingXmlFolder;
            LogFile = logFile;

            if (!Environment.UserInteractive)
                // running as service
                using (var service = new MonitorService())
                    ServiceBase.Run(service);
            else
            {
                // running as console app
                Run();

                Console.WriteLine("Enter 'q' to exit.");
                while (Console.Read() != 'q');
            }
        }
        public static void Run()
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = InComingXmlFolder;

            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            watcher.Filter = "*.xml";

            watcher.Created += OnChanged;

            watcher.EnableRaisingEvents = true;
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            XElement element = XElement.Parse(File.ReadAllText(e.FullPath));
            IEnumerable<XElement> people = element.Elements();

            foreach (var person in people)
            {
                var personModel = new PersonModel();

                var firstName = person.Element("firstname").Value;
                var surname = person.Element("surname").Value;
                var email = person.Element("email").Value;
                var referenceno = person.Element("referenceno").Value;

                personModel.Surname = surname;
                personModel.FirstName = firstName;
                personModel.ReferenceNo = Guid.Parse(referenceno);
                personModel.Email = email;

                var orders = person.Elements("orders").Elements();

                var ordersModels = (from order in orders
                                    let orderreference = order.Element("orderreference").Value
                                    let ordername = order.Element("ordername").Value
                                    let ordervalue = order.Element("ordervalue").Value
                                    select new OrderModel
                                    {
                                        ReferenceNo = Guid.Parse(orderreference),
                                        PersonReferenceNo = Guid.Parse(referenceno),
                                        Name = ordername,
                                        Value = Convert.ToDecimal(ordervalue)
                                    }).ToList();

                _marketersRepository.AddPersonWithOrders(personModel, ordersModels);

                string logPerson = $"Person:\n firstname = {personModel.FirstName}\n surname = {personModel.Surname}\n email = {personModel.Email}\n referenceno = {referenceno}\n\n";
                File.AppendAllText(LogFile, logPerson);
            }
        }

    }
}
