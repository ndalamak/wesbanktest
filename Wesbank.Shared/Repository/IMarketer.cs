﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wesbank.Shared.Models;

namespace Wesbank.Shared.Repository
{
    public interface IMarketer
    {
        List<PersonModel> GetPeople();
        PersonModel AddPersonWithOrders(PersonModel personModel, List<OrderModel> orderModels);
    }
}
