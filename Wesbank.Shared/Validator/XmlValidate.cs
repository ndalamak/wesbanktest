﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace Validator
{
    public class XmlValidate
    {
        bool _invalide;

        public bool ValidateXml(Stream xmlFile, string validationFile)
        {
            _invalide = true;
            XmlSchemaSet schemaSet = new XmlSchemaSet();
            schemaSet.Add("", validationFile);

            Validate(xmlFile, schemaSet);

            return _invalide;
        }

        private void Validate(Stream filename, XmlSchemaSet schemaSet)
        {
            XmlSchema compiledSchema = null;

            foreach (XmlSchema schema in schemaSet.Schemas())
            {
                compiledSchema = schema;
            }

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.Schemas.Add(compiledSchema);
            settings.ValidationEventHandler += ValidationCallBack;
            settings.ValidationType = ValidationType.Schema;

            //XmlReader vreader = XmlReader.Create(filename, settings);
            XmlReader vreader = XmlReader.Create(filename, settings);
            while (vreader.Read()) { }

            vreader.Close();
        }
        
        private void ValidationCallBack(object sender, ValidationEventArgs args)
        {
            _invalide = false;
        }
    }
}
