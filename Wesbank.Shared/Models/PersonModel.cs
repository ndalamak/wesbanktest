﻿using System;
using System.Collections.Generic;

namespace Wesbank.Shared.Models
{
    public class PersonModel
    {
        public Guid ReferenceNo { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public List<OrderModel> Orders { get; set; }
    }
}
