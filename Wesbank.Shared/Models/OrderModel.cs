﻿using System;

namespace Wesbank.Shared.Models
{
    public class OrderModel
    {
        public Guid ReferenceNo { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public Guid PersonReferenceNo { get; set; }
    }
}
