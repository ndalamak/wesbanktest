﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Wesbank.Repository.Helpers;
using Wesbank.Shared.Models;
using Wesbank.Shared.Repository;

namespace Wesbank.Repository.Marketers
{
    public class Repository : IMarketer
    {
        private MarketersHelper _helper;

        public Repository()
        {
            _helper = new MarketersHelper();
        }

        public List<PersonModel> GetPeople()
        {
            using (var db = new wesBankEntities())
            {
                return db.People.Include(o=>o.Orders).ToList().Select(p=> _helper.PersonModelFromPerson(p)).ToList();
            }
        }

        public PersonModel AddPersonWithOrders(PersonModel personModel, List<OrderModel> orderModels )
        {
            var person = _helper.PersonFromModel(personModel);

            var orders = new List<Order>();
            foreach (var orderModel in orderModels)
            {
                var order = _helper.OrderFromModel(orderModel);
                orders.Add(order);
            }

            using (var db = new wesBankEntities())
            {
                var dbperson = db.People.SingleOrDefault(p => p.ReferenceNo == person.ReferenceNo);
                if (dbperson == null)
                {
                    db.People.Add(person);
                }
                else
                {
                    dbperson.Email = person.Email;
                    dbperson.FirstName = person.FirstName;
                    dbperson.Surname = person.Surname;
                }

                foreach (var order in orders)
                {
                    //this can be optimised
                    var dborder = db.Orders.SingleOrDefault(p => p.ReferenceNo == order.ReferenceNo);
                    if (dborder == null)
                    {
                        db.Orders.Add(order);
                    }
                    else
                    {
                        dborder.Name = order.Name;
                        dborder.Value = order.Value;
                        dborder.PersonReferenceNo = order.PersonReferenceNo;
                    }
                }
                db.SaveChanges();

                return _helper.PersonModelFromPerson(person);
            }
        }
    }
}
