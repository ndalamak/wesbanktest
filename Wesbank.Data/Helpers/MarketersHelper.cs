﻿using System.Linq;
using Wesbank.Shared.Models;

namespace Wesbank.Repository.Helpers
{
    public class MarketersHelper
    {
        public Person PersonFromModel(PersonModel personModel)
        {
            var person = new Person
            {
                Email = personModel.Email,
                FirstName = personModel.FirstName,
                ReferenceNo = personModel.ReferenceNo,
                Surname = personModel.Surname
            };

            return person;
        }

        public Order OrderFromModel( OrderModel orderModel)
        {
            var order = new Order
            {
                Name = orderModel.Name,
                PersonReferenceNo = orderModel.PersonReferenceNo,
                ReferenceNo = orderModel.ReferenceNo,
                Value = orderModel.Value
            };

            return order;
        }

        public PersonModel PersonModelFromPerson(Person person)
        {
            var personModel = new PersonModel
            {
                Email = person.Email,
                FirstName = person.FirstName,
                ReferenceNo = person.ReferenceNo,
                Surname = person.Surname,
                Orders = person.Orders.Select(OrderModelFrom).ToList()
            };

            return personModel;
        }

        public  OrderModel OrderModelFrom(Order order)
        {
            var modelOrder = new OrderModel
            {
                Name = order.Name,
                PersonReferenceNo = order.PersonReferenceNo.Value,
                ReferenceNo = order.ReferenceNo,
                Value = order.Value ?? 0
            };

            return modelOrder;
        }
    }
}
