﻿angular.module('upload', [])
    .controller('uploadCtrl', ['$scope', '$http', 'Notification', function ($scope, $http, notification) {
        $scope.files = [];
        $scope.addFile = function () {
            $scope.files.push($scope.files.length);
        }
        $scope.addFile();

        $scope.uploadFiles = function () {
            var f = document.getElementById('uploadfile').files;

            for (var i = 0; i < f.length; i++) {
                var r = new FileReader();
                r.fileName = f[i].name;
                r.onloadend = function (e) {
                    var data = e.target.result;
                    var filename = e.target.fileName;
                    var params = {
                        Data: data,
                        FileName: filename
                    };
                    $http.post('/api/Basic/UploadXmlFile', params)
                       .success(function () {
                           $("#uploadfile").val('');
                           notification.success(filename + ' was uploaded.');
                       })
                       .error(function (data) {
                           var errorMessages;
                           if (angular.isArray(data))
                               errorMessages = data;
                           else
                               errorMessages = new Array(data.replace(/["']{1}/gi, ""));


                           errorMessages.forEach(function (error) {
                               notification.error(filename + ': ' + error);
                           });
                       });
                };
                r.readAsDataURL(f[i]);
            }
        }
    }
    ]);