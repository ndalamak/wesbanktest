﻿angular.module('people', [])
    .controller('peopleCtrl', ['$scope', '$http', 'Notification', '$filter', function ($scope, $http, notification, $filter) {

        $scope.getPeople = function () {
            $http.get('/api/Basic/GetPeople')
                .success(function (data, status, headers, config) {
                    $scope.people = data;
                }).error(function (data, status, headers, config) {
                    $scope.message = data.ExceptionMessage + '<br>' + data.Message;
                    notification.error($scope.message);
                });
        }

        $scope.showModal = function(ref){
            $scope.orders = $filter('filter')($scope.people, function (d) { return d.ReferenceNo === ref; })[0].Orders;
        }

        $scope.getPeople();
    }]);