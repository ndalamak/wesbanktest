﻿var app = angular.module('app', [
    'ngRoute',
    'ngLoadingSpinner',
    'ui-notification',
    'upload',
    'home',
    'people'
]);

app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider.when('/home', {
        templateUrl: 'App/home',
        controller: 'homeCtrl'
    });

    $routeProvider.when('/upload', {
        templateUrl: 'App/Upload',
        controller: 'uploadCtrl'
    });

    $routeProvider.when('/people', {
        templateUrl: 'App/People',
        controller: 'peopleCtrl'
    });

    $routeProvider.otherwise({
        redirectTo: '/home'
    });
}]);
