﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Wesbank.Web.Models
{
    public class FileUploadModel
    {
        [Required(ErrorMessage = "The Task Field is Required.")]
        [RegularExpression("^.*\\.([xX][mM][lL])$", ErrorMessage = "only XML files allowed")]
        public string FileName { get; set; }
        [Required(ErrorMessage = "The file is required")]
        public string Data { get; set; }
    }
}