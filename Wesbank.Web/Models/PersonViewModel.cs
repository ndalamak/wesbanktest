﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wesbank.Web.Models
{
    public class PersonViewModel
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public Guid ReferenceNo { get; set; }
        public virtual List<OrderViewModel> Orders { get; set; }
    }
}