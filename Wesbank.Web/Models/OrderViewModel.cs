using System;

namespace Wesbank.Web.Models
{
    public class OrderViewModel
    {
        public Guid ReferenceNo { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public Guid PersonReferenceNo { get; set; }
    }
}