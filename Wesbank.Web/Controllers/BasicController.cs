﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Validator;
using Wesbank.Repository;
using Wesbank.Shared.Models;
using Wesbank.Shared.Repository;
using Wesbank.Web.Models;

namespace Wesbank.Web.Controllers
{
    public class BasicController : ApiController
    {
        private IMarketer _marketerRepository;
        public BasicController(IMarketer marketer)
        {
            _marketerRepository = marketer;
        }

        [HttpGet]
        public List<PersonViewModel> GetPeople()
        {
            var people = _marketerRepository.GetPeople();
            return GetPeopleViewModel(people);
        }

        [HttpPost]
        public HttpResponseMessage UploadXmlFile(FileUploadModel model)
        {
            if (ModelState.IsValid)
            {
                var folder = ConfigurationManager.AppSettings["Incoming_XML_Folder"];
                var validatorFile = ConfigurationManager.AppSettings["XML_XSD_Validator"];

                var file = Convert.FromBase64String(model.Data.Substring(model.Data.IndexOf(',') + 1));

                Stream stream = new MemoryStream(file);
                var validator = new XmlValidate();
                if (validator.ValidateXml(stream, validatorFile))
                {
                    var path = Path.Combine(folder, model.FileName);
                    File.WriteAllBytes(path, file);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "XML Validation failed.");
                }
            }
            else
            {
                IEnumerable<string> errors = ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage);
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, errors);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private List<PersonViewModel> GetPeopleViewModel(List<PersonModel> people)
        {
            var results = new List<PersonViewModel>();
            foreach (var person in people)
            {
                var orders = new List<OrderViewModel>();
                foreach (var order in person.Orders)
                {
                    orders.Add(new OrderViewModel
                    {
                        ReferenceNo = order.ReferenceNo,
                        PersonReferenceNo = order.PersonReferenceNo,
                        Name = order.Name,
                        Value = order.Value
                    });
                }

                results.Add(new PersonViewModel
                {
                    ReferenceNo = person.ReferenceNo,
                    FirstName = person.FirstName,
                    Email = person.Email,
                    Surname = person.Surname,
                    Orders = orders
                });
            }

            return results;
        }
    }
}
