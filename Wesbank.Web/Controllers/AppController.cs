﻿using System.Web.Mvc;

namespace Wesbank.Web.Controllers
{
    public class AppController : Controller
    {
        public ActionResult Home()
        {
            return PartialView();
        }

        public ActionResult Upload()
        {
            return PartialView();
        }
        public ActionResult People()
        {
            return PartialView();
        }
    }
}